#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import sys
import select
from uuid import getnode

myMac=getnode()

"""
    !@file  Script to discover Orvibo S-20 devices. 
"""

#
# set up a helper for logging
#
chatty=0
important=1
critical=2
requiredPri=chatty   # 0 => most verbose

def isprint(c):
    return c>=32 and c<=126

def dump(byteStr):
    """format and print binary data to console"""
    i=0;
    charStr=""
    for x in byteStr:
        print("{:3x} ".format(x), end='', sep='')
        if isprint(x):
            charStr = charStr+chr(x)
        else:
            charStr = charStr+'.'
        i+=1
        if i == 8:
            i=0
            print(" [{}]".format(charStr))
            charStr = ""
    if i != 8:
        print(" [{}]".format(charStr))
    print("\n ")
    
def logMsg( pri, str, byteStr=None):
    """Print out message if it meets the required priority"""
    if pri >= requiredPri:
        print(str)
        if byteStr != None:
            dump(byteStr)

def toBytes(val,count):
    """Convert long long to string of bytes (MAC addr)"""
    b=bytes()
    for i in range(0, count):
        b=bytes([val%256])+b
        val = val//256
        print( i, b)
    return b


toBytes(5,3)
mac=toBytes(getnode(),6)
dump(mac)
#
# check command line arguments
#
if len(sys.argv) < 2:
    print("Usage: ", sys.argv[0], "IP")
    sys.exit(-1)
else:
    args = "IP \"" + sys.argv[1] + "\""
    logMsg(important, args)

# Extract command line args
sendIP = sys.argv[1]

BCAST_PORT = 10000

class sendS20:
    """Exchange message with Orvibo S20"""
    def __init__(self, IP):
        """Save parameters, open the socket and set socket options"""
        self.ipAddr = IP
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        #self.sock.setblocking(0) # non-blocking so select() will work.
        self.sock.bind((IP, BCAST_PORT-1))

    def send(self, msg):
        """Send a message on ths socket and read the reply"""
        self.sock.sendto(msg, (self.ipAddr, BCAST_PORT))
        ready = select.select([self.sock], [], [], 1)
        if ready[0]:
            reply, addr = self.sock.recvfrom(1024)
            logMsg(important, "Sent ... got:", reply)
            return reply
        else:
            logMsg(important, "Sent ... got nothing")
            return (None)

    def recv(self):
        """Receive a message on ths socket"""
        ready = select.select([self.sock], [], [], 2)
        if ready[0]:
            reply, addr = self.sock.recvfrom(1024)
            logMsg(important, "Receive ... got:", reply)
            return reply
        else:
            logMsg(important, "Receive ... got nothing")
            return None

discovery = bytes([0x68, 0x64, 0x00, 0x12, 0x71, 0x67, 0xac, 0xcf,\
                   0x23, 0x24, 0x19, 0xc0, 0x20, 0x20, 0x20, 0x20,\
                   0x20, 0x20])

dump(discovery)
s20 = sendS20(sendIP)
s20.send(discovery)
s20.recv()
