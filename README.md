# README #

This is yet more code to support the Orvibo S20 WiFi switch (AKA
Aldi/Bauhn Wifi Power Point)

## What is this repository for? ##

* Python scripts to manage an Orvibo S20 WiFi socket
* Version 00.01


## Purpose
This code is intended to serve two purposes:
1.  Allow me to turn the switch on and off from my laptop.
2.  Use Python for something useful.

## Functionality ##
The code needs to serve two functions. First is to 'pair' the socket with
a WiFi access point. The second is to send control messages to the socket.

## Future
This code is more or less abandoned. Pairing still works. I've used
code from https://github.com/happyleavesaoc/python-orvibo to handle the
on/off. Except it doesn't work on one of myt hosts when running Ubuntu
MATE. Rather than debug that (which I may do anyway) I hear the siren song
of the Go programming language so I'm working on a replacement written
in Go. It can be found at https://github.com/HankB/orvibo. That project
has reached the maturity level of this and I intend to proceed with it.

### Pairing 
Status: working

Pairing us accomplished by pressing the button on the socket for 4 seconds
twice (until the indicator flashes blue at about 3 Hz.) When that is done,
the S20 operates as an open access point and a host can associate with
it and initiate the pairing operation which sends the S20 an SSID and
password to use on the user's access point.

### Discovery
Status:started

Identify the S20s on the local LAN.

### Subscription
Status:none

Must be performed prior to any control.

### Control
Status:none
Turning the darn thing on and off and maybe setting timers.

### Emulation
Status: partial

This Python script is supposed to play the other side of the conversation
and allow easy testing of the other scripts without the need to involve
an actual Orvibo S20. Of course things could work with the emulation
that don't actually work with the real S20.


## My setup ##
* Orvibo S20
* TP-LINK TL-WN725N (USB WiFi) (or any WiFi adapter.)
* My AP with capability to support guest network.

### How do I get set up? ###

* Plug in the S20. It should flash red at about 5 Hz/
* press button/LED indicator for ~4 seconds should now flash blue at about 5 Hz
* Associate with access point WiWo-S20
* Determine IP address for WiWo-S20 interface
* Run S20-pair.py [IP] [SSID] [password]

(IP is the broadcast address for the network that results from associating
with the WiWo-S20) (The previous statement is wrong. The IP address
the S20 uses when in AP mode is "10.10.100.254" and it will assign
"10.10.100.150" to the first host to connect. Pairing takes all of
a fraction of a second and it makes little sense to not make these
program constants.

## Opportunities to contribute ##
(In my perceived order of importance.)

* Code review - My most significant Python project to date. I have much to learn. ;)
* Writing tests
* Additional documentation
* Additional functionality.

### Who do I talk to? ###

* Me!

This is my first open source project. Don't be afraid to explain things to me. ;)
Thank you for reading this far!

## Other
I gave some thought to using 2.7.6 vs. 3.4.3, the two versions available
on my Mint 17.2 setup. I don't think the situation is quite as bad as
Perl6/Parrot. (I understand Parrot is dead, not just pining...) I do
not have any legacy code to support so the incompatibility is not an
issue. This code will deal extensively with byte arrays and some strings
and that is one of the things that 3 is supposed to improve upon. Python
3 it is!
