#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    !@file Simulation of the S20 used for development of an S20 control S/W.
    This emulation covers the pairing process when the S20 is put in AP mode 
    (rapid blue flashing indicator)
"""
import socket
UDP_IP = "127.0.0.1"
UDP_PORT = 48899

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
print("     Socket:", sock)

sock.bind((UDP_IP, UDP_PORT))

while True:
    data, addr = sock.recvfrom(1024)
    print("received", data.rstrip())
    print("from", addr)
    dataString = data.decode('UTF-8')
    if data == bytes("HF-A11ASSISTHREAD", 'UTF-8'):     # asking to pair?
        sock.sendto( bytes("127.0.0.1,11:22:33:44:55,S20", 'UTF-8'), (UDP_IP, addr[1]))
        print("Got first message, sending IP,MAC,HOSTNAME")
        print()
    elif bytes("AT+WSSSID", 'UTF-8') in data:           # got SSID?
        dataString=dataString.split("=")
        ssid=dataString[1][0:len(dataString[1])-1]
        print("got SSID \"", ssid, "\"sending OK")
        print()
        sock.sendto( bytes("+ok\r\r", 'UTF-8'), (UDP_IP, addr[1]))
    elif bytes("AT+WSKEY", 'UTF-8') in data:            # got SSID?
        dataString=dataString.split("=")
        sec,enc,pwd=dataString[1].split(',')
        print("got PWD \"", pwd, "\" sending OK")
        print()
        sock.sendto( bytes("+ok\r\r", 'UTF-8'), (UDP_IP, addr[1]))
    elif bytes("AT+WMODE", 'UTF-8') in data:            # got SSID?
        print("got STA")
        print()
        sock.sendto( bytes("+ok\r\r", 'UTF-8'), (UDP_IP, addr[1]))
    else:
        print("Unhandled \"", data.rstrip(), " \"\r")
