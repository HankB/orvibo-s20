#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import sys
import select

"""
    !@file  Script to 'pair' an Orvibo S-20. The 'pair' operation consists of providing
            the SSID and password so the S20 can join the local WiFi network.
"""

#
# set up a helper for logging
#
chatty=0
important=1
critical=2
requiredPri=chatty   # 0 => most verbose

def logMsg( pri, str, byteStr=None):
    """Print out message if it meets the required priority"""
    if pri >= requiredPri:
        if byteStr == None:
            print(str)
        else:
            print(str, byteStr.decode('UTF-8').rstrip())


#
# check command line arguments
#
if len(sys.argv) < 3:
    print("Usage: ", sys.argv[0], " IP SSID PWD")
    sys.exit(-1)
else:
    args = "IP \"" + sys.argv[1] + " SSID \"" + sys.argv[2]  \
        + "\" PWD \"" + sys.argv[3] + "\""
    logMsg(important, args)

# Extract command line args
sendIP = sys.argv[1]
ssid = sys.argv[2]
pwd = sys.argv[3]

UDP_RCV_PORT = 9884     # port we listen on
UDP_SND_PORT = 48899    # port S20 listens on

class pairS20:
    """Pair with Orvibo S20"""
    def __init__(self, IP, SSID, PWD):
        """Save parameters, open the socket and set socket options"""
        self.ipAddr = IP
        self.ssid = SSID
        self.password = PWD
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        #self.sock.setblocking(0) # non-blocking so select() will work.

    def send(self, msg):
        """Send a mssage on ths socket and read the reply"""
        self.sock.sendto(msg.encode('UTF-8'), (self.ipAddr, UDP_SND_PORT))
        ready = select.select([self.sock], [], [], 1)
        if ready[0]:
            reply, addr = self.sock.recvfrom(1024)
            logMsg(important, "Sent \""+msg.rstrip()+"\" got:", reply)
            return reply
        else:
            logMsg(important, "Sent \""+msg.rstrip()+"\" got nothing")
            return None
 
# create our socket object
conn = pairS20(sendIP, ssid, pwd)

# Initiate conversation
reply = conn.send("HF-A11ASSISTHREAD")
if reply == None:
    logMsg(critical, "not successful")
    sys.exit(-1)

conn.send("+ok") # our turn to reply

# send SSID
reply = conn.send("AT+WSSSID=" + ssid + "\r")
if reply == None or "+ERR" in reply.decode('UTF-8'):
    logMsg(critical, "not successful", reply)
    sys.exit(-1)

# send PWD
reply = conn.send("AT+WSKEY=WPA2PSK,AES," + pwd + "\n")
if reply == None or "+ERR" in reply.decode('UTF-8'):
    logMsg(critical, "not successful", reply)
    sys.exit(-1)

# send STA
reply = conn.send("AT+WMODE=STA\n")
if reply == None or "+ERR" in reply.decode('UTF-8'):
    logMsg(critical, "not successful", reply)
    sys.exit(-1)

# send Reboot
reply = conn.send("AT+Z\n")
if reply == None:
    logMsg(critical, "no reply, presumed successful")
elif "+ERR" in reply.decode('UTF-8'):
    logMsg(critical, "not successful", reply)
    sys.exit(-1)


sys.exit(0)
#
#
print("Sending \"AT+Z\r"," to ", sendIP, UDP_SND_PORT)
sock.sendto("AT+Z\r", (sendIP, UDP_SND_PORT)) # Send SSID
reply, addr = sock.recvfrom(1024)
print("Received:", reply, " from ", addr)
if "+ERR" in reply:
    print("Received +ERR from S20 - exiting")
    sys.exit(-1)







